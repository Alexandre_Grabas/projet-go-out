<?php

namespace App\Notification;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Security\Core\User\UserInterface;

class Sender
{


    public function __construct(private readonly MailerInterface $mailer)
    {
    }

    public function sendNewUserNotificationToAdmin(UserInterface $user):void{
        //pour tester
        file_put_contents('debug.txt',$user->getEmail());

        $message = new Email();
        $message->from('goout@goout.fr')
            ->to('admin@goout.fr')
            ->subject('Nouvel Utilisateur créé depuis le site goout')
            ->html('<h1>Nouvel Utilisateur</h1>Email: '.$user->getEmail());

        $this->mailer->send($message);


    }
}