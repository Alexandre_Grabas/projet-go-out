<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\SiteRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\Reader;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
class ImportCsvUsersService
{
    public function __construct(
        private UserRepository $userRepository,
        private SiteRepository $siteRepository,
        private EntityManagerInterface $em,
        private UserPasswordHasherInterface $userPasswordHasherInterface
    )
    {

    }

    public function importUsers(SymfonyStyle $io){
        $io->title('Importation des utilisateurs en cours');
        $users = $this->readCsvFile();


        $io->progressStart(count($users));
        $userCreated = 0;

        foreach ($users as  $arrayUser){
            //dd($arrayUser);//test

            /*
             // ... e.g. get the user data from a registration form
                $user = new User(...);
                $plaintextPassword = ...;

            // hash the password (based on the security.yaml config for the $user class)
                $hashedPassword = $passwordHasher->hashPassword(
                $user,
                $plaintextPassword
            );
                $user->setPassword($hashedPassword);
             * */
            $io->progressAdvance();
            $user = $this->createOrUpdateUser($arrayUser);

            $this->em->persist($user);
            $this->em->flush();

            ++$userCreated;
        }
        //$this->em->flush();

        $io->progressFinish();
        $io->success('Importation terminée');

    }
    private function  readCsvFile():Reader{
        //pour test
        //$targetCvsDirectory='%kernel.project_dir%/public/uploads/csv';
        $fileName="users.csv";
        $rep="%kernel.root_dir%/../public/uploads/csv";

        //email,roles,password,firstname,lastname,phone,active,site,isVerified,theme,filename
        //load the CSV document from a file path
        $csv = Reader::createFromPath($rep.'/'.$fileName, 'r');
        $csv->setDelimiter(',');
        $csv->setHeaderOffset(0);

        return $csv;

    }
    private function createOrUpdateUser (array $arrayUser): User{
        $user = $this->userRepository->findOneBy(['email'=>$arrayUser['email']]);
        if(!$user){
            //s'il n'existe pas on le créé
            $user=new User();
        }

        $hashedPassword = $this->userPasswordHasherInterface->hashPassword($user,$arrayUser['password']);
        //email,roles,password,firstname,lastname,phone,active,site,isVerified,theme,filename=non modifié
        $user->setEmail($arrayUser['email'])
            ->setRoles([$arrayUser['roles']])
            ->setPassword($hashedPassword)
            ->setFirstname($arrayUser['firstname'])
            ->setLastname($arrayUser['lastname'])
            ->setPhone($arrayUser['phone'])
            ->setActive($arrayUser['active'])
            ->setSite($this->siteRepository->findOneBy(['name'=>$arrayUser['site']]))
            ->setIsVerified($arrayUser['isVerified'])
            ->setTheme($arrayUser['theme']);
        return $user;
    }

    //invoke if you want to delete file after import
    private function delete(?string $filename, string $rep): void
    {
        if (null != $filename) {
            if (file_exists($rep . '/' . $filename)) {
                unlink($rep . '/' . $filename);
            }
        }
    }
}