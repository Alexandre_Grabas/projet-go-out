<?php

namespace App\Form;

use App\Entity\Site;
use App\Entity\User;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $builder
            ->add('email',EmailType::class,[
                'required' => true,
                    ])
            ->add('firstname',TextType::class,[
                'required' => true,

                ])
            ->add('lastname',TextType::class,[
                'required' => true,
                ])
            ->add('phone',TextType::class,[
                'required' => true,
                ])

            ->add('site',EntityType::class,[
                'required' =>true,
                'label' => 'Site',
                'class' => Site::class,
                'choice_label' => 'name',
            ])
            ->add('plainPassword', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'attr' => ['autocomplete' => 'new-password'],
                'label' => 'Mot de passe ',
            ])
            ->add('nouveauPassword', RepeatedType::class,[
                'type' => PasswordType::class,
                'mapped' => false,
                'label' => 'Modificationc mot de passe',
                'required' => false,
            ])
//            ->add('password',PasswordType::class, [
//                'always_empty' => 'true',
//                'required' => 'true',
//
//            ])
            ->add('roles', ChoiceType::class, [
                    'multiple' => false,
                    'expanded' => false,
                    'choices'  => [
                        'User' => 'ROLE_USER',
                        'Admin' => 'ROLE_ADMIN',
                    ],
                ])
            ->add('image',FileType::class,
                [
                    'mapped'=>false,
                    'required'=>false,
                    'constraints'=> [
                        new Image([
                            'maxSize' => '1024k',
                            'mimeTypes' => [
                                'image/jpeg',
                                'image/png',
                            ],
                            'mimeTypesMessage' => 'Please upload a valid image',
                        ])
                    ]
                ]
            )
            ->add('active')
            ->add('isVerified')
            ->add('theme',ChoiceType::class,[
                'required' => true,
                'multiple' => false,
                'expanded' => false,
                'choices'  => [
                    'Lightblue' => 'lightblue',
                    'Flower' => 'flower',
                    'Kevin' => 'kevin'
                ],
            ])
        ;
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent
                                                                       $event) {
            $user = $event->getData();
            if ($user && $user->getFilename()) {
            // cas où on est en modification et qu'une image est déjà présente,
            // on ajoute un checkbox pour permettre de demander
            // la suppression de l'image
                $form = $event->getForm();
                $form->add('deleteImage', CheckboxType::class, [
                    'required' => false,
                    'mapped' => false,
                ]);
            }
        });

        $builder->get('roles')
            ->addModelTransformer(new CallbackTransformer(
                function ($rolesArray) {
                    // transform the array to a string
                    return count($rolesArray)? $rolesArray[0]: null;
                },
                function ($rolesString) {
                    // transform the string back to an array
                    return [$rolesString];
                }
            ));
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
