<?php

namespace App\Form;

use App\Entity\Site;
use App\Entity\Sortie;
use App\Entity\Ville;
use App\Repository\VilleRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SortieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('dateHourStart', DateTimeType::class, array(
                'widget' => 'single_text',
                'input' => 'datetime_immutable'))
            ->add('duration')
            ->add('deadlineSubscription', DateTimeType::class, array(
                'widget' => 'single_text',
                'input' => 'datetime_immutable',
                ))
            ->add('maxSubscription')
            ->add('description')
            ->add('lieu')
            /*->add('ville',EntityType::class,[
                'mapped'=>false,
                'required' =>true,
                'label' => 'Ville',
                'class' => Ville::class,
                'choice_label' => 'name',
            ])*/
            //->add('site')
            //->add('participants')
            //->add('owner')
            //->add('etat')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Sortie::class,

        ]);
    }

}
