<?php

namespace App\Controller;

use App\Entity\Sortie;
use App\Form\SortieType;
use App\Repository\EtatRepository;
use App\Repository\SiteRepository;
use App\Repository\SortieRepository;
use App\Repository\UserRepository;
use App\Repository\VilleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use function Sodium\add;

#[Route('/app/sortie')]
class SortieController extends AbstractController
{
    #[Route('/', name: 'app_sortie_index', methods: ['GET','POST'])]
    public function index(SortieRepository $sortieRepository, SiteRepository $siteRepository): Response
    {
        $sites=$siteRepository->findAll();
        $sortiesTemp=array();
        $sorties=$sortieRepository->findAll();

        if (!in_array( 'ROLE_ADMIN' , $this->getUser()->getRoles())) {
            foreach ($sorties as $sortie) {
                if (new \DateTimeImmutable("now") > $sortie->getDeadlineSubscription()) {
                    $datedif = (new \DateTimeImmutable("now"))->diff($sortie->getDeadlineSubscription());
                    if (($datedif->format('%y') <= 1)) {
                        if ($datedif->format('%m') <= 1) {
                            array_push($sortiesTemp, $sortie);
                        }
                    }
                }else{
                    array_push($sortiesTemp, $sortie);
                }

            }
            $sorties = $sortiesTemp;
        }

        //variable filter
        $owner='';
        $subscribed='';
        $notSubscribed='';
        $ended='';
        $siteFiltre='';
        $textFiltre='';
        $dateUp='';
        $dateDown='';


        //function filter
        if (isset($_POST['owner']) || isset($_POST['subscribed']) || isset($_POST['notSubscribed']) || isset($_POST['ended']) || (isset($_POST['site'])&& $_POST['site']!="--Sélectioner un lieu--") || (isset($_POST['textFiltre'])&& $_POST['textFiltre']!="") || (isset($_POST['dateUp'])&& $_POST['dateUp']!="") || (isset($_POST['dateDown'])&& $_POST['dateDown']!="")){
            $sortiesFiltred=array();
            $ajout=false;

            //filter check box
            foreach ($sorties as $sortie) {
                if (isset($_POST['owner'])) {
                    $owner = $_POST['owner'];
                    if (!$ajout) {
                        if ($sortie->getOwner() == $this->getUser()) {
                            $ajout = true;

                        }
                    }
                }
                if (isset($_POST['subscribed'])) {
                    $subscribed = $_POST['subscribed'];
                    if (!$ajout) {
                        foreach ($sortie->getParticipants() as $participant)
                            if ($this->getUser() == $participant) {
                                $ajout = true;
                                break;
                            }
                    }
                }
                if (isset($_POST['notSubscribed'])) {
                    $notSubscribed = $_POST['notSubscribed'];
                    if (!$ajout) {
                        $temp = true;
                        foreach ($sortie->getParticipants() as $participant)
                            if ($this->getUser() == $participant) {
                                $temp = false;
                                break;
                            }
                        if ($temp) {
                            $ajout = true;
                        }
                    }
                }
                if (isset($_POST['ended'])) {
                    $ended = $_POST['ended'];
                    if (!$ajout) {
                        if ($sortie->getDateHourStart() < new \DateTimeImmutable("now")) {
                            $ajout = true;
                        }
                    }
                }
                if ($ajout) {
                    array_push($sortiesFiltred, $sortie);
                    $ajout = false;
                }
            }
            //end filter checkbox

            //filter site
            if (isset($_POST['site']) && $_POST['site'] != "--Sélectioner un lieu--") {
                $sorties=$sortiesFiltred;
                $sortiesFiltred=array();
                $siteFiltre = $_POST['site'];
                foreach ($sorties as $sortie) {
                    if (!$ajout) {
                        if ($sortie->getSite() == $_POST['site']) {
                            $ajout = true;
                        }
                    }

                    if ($ajout) {
                        array_push($sortiesFiltred, $sortie);
                        $ajout = false;
                    }
                }
            }

            //filter text
            if (isset($_POST['textFiltre']) && $_POST['textFiltre'] != "") {
                $sorties = $sortiesFiltred;
                $sortiesFiltred = array();
                $textFiltre = $_POST['textFiltre'];
                foreach ($sorties as $sortie) {
                    if (!$ajout) {
                        if (str_contains($sortie->getName(), $_POST['textFiltre'])) {
                            $ajout = true;
                        }
                    }

                    if ($ajout) {
                        array_push($sortiesFiltred, $sortie);
                        $ajout = false;
                    }
                }
            }

            //filter date up
            if (isset($_POST['dateUp']) && $_POST['dateUp'] != "") {
                $sorties = $sortiesFiltred;
                $sortiesFiltred = array();
                $dateUp = $_POST['dateUp'];
                foreach ($sorties as $sortie) {
                    if (!$ajout) {
                        if ($sortie->getDateHourStart() <= new \DateTimeImmutable($_POST['dateUp'])) {

                            $ajout = true;
                        }
                    }

                    if ($ajout) {
                        array_push($sortiesFiltred, $sortie);
                        $ajout = false;
                    }
                }
            }

            //filter date down
            if (isset($_POST['dateDown']) && $_POST['dateDown'] != "") {
                $sorties = $sortiesFiltred;
                $sortiesFiltred = array();
                $dateDown = $_POST['dateDown'];
                foreach ($sorties as $sortie) {
                    if (!$ajout) {
                        if ($sortie->getDateHourStart() >= new \DateTimeImmutable($_POST['dateDown'])) {
                            $ajout = true;
                        }
                    }

                    if ($ajout) {
                        array_push($sortiesFiltred, $sortie);
                        $ajout = false;
                    }
                }
            }
            $sorties=$sortiesFiltred;
        //cas aucun filtre rentrer
        }else{
            //cas 1er visite page
            if (!isset($_POST['boutonFiltrer'])) {
                $owner = 'on';
                $subscribed = 'on';
                $notSubscribed = 'on';
                $ended = 'on';
            //cas aucun filtre rentrer et soumission des filtres
            }else{
                $sorties=array();
            }
        }


        return $this->render('sortie/index.html.twig', [
            'sorties' => $sorties,
            'sites' => $sites,
            'owner' => $owner,
            'subscribed' => $subscribed,
            'notSubscribed' => $notSubscribed,
            'ended' => $ended,
            'siteFiltre' => $siteFiltre,
            'textFiltre' => $textFiltre,
            'dateUp' => $dateUp,
            'dateDown' => $dateDown,
        ]);
    }

    #[Route('/new', name: 'app_sortie_new', methods: ['GET', 'POST'])]
    public function new(Request $request, SortieRepository $sortieRepository,
                        UserRepository $userRepository,
                        EtatRepository $etatRepository,
                        VilleRepository $villeRepository,
                        EntityManagerInterface $em
                        ): Response
    {
        $sortie = new Sortie();
        $form = $this->createForm(SortieType::class, $sortie);
        $form->handleRequest($request);
        $villes = $villeRepository->findAll();

        if ($form->isSubmitted() && $form->isValid()) {

            $etat = $etatRepository->findOneBy(array('libelle'=>"Créée"));
            $owner = $userRepository->find($this->getUser()->getId());
            $site = $owner->getSite();

            $sortie->setSite($site);
            $sortie->setOwner($owner);
            $sortie->setEtat($etat);

            $sortieRepository->save($sortie, true);
            if(isset($_POST["publier"]) && $sortie->getEtat()=='Créée'){
                $sortie->setEtat($etatRepository->findOneBy(['libelle'=>'Ouverte']));
                $em->persist($sortie);
                $em->flush();
            }
            return $this->redirectToRoute('app_sortie_index', [], Response::HTTP_SEE_OTHER);
        }
        //TODO add listener on villes to link lieu by villes
        return $this->renderForm('sortie/new.html.twig', [
            'sortie' => $sortie,
            'form' => $form,
            'villes' => $villes,
        ]);
    }

    #[Route('/{id}', name: 'app_sortie_show', methods: ['GET'])]
    public function show(Sortie $sortie): Response
    {
        return $this->render('sortie/show.html.twig', [
            'sortie' => $sortie,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_sortie_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Sortie $sortie, SortieRepository $sortieRepository,VilleRepository $villeRepository, EtatRepository $etatRepository, EntityManagerInterface $em): Response
    {

        $villes = $villeRepository->findAll();
        if($this->getUser() != null){
            // Check if the connected user is the owner or is admin
            if ( in_array( 'ROLE_ADMIN' , $this->getUser()->getRoles()) || $this->getUser()->getId() == $sortie->getOwner()->getId()){
                if($sortie->getEtat()!='Créée' and (!in_array( 'ROLE_ADMIN' , $this->getUser()->getRoles()))){
                    return $this->redirectToRoute('app_sortie_index', [], Response::HTTP_SEE_OTHER);
                }
                $form = $this->createForm(SortieType::class, $sortie);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $sortieRepository->save($sortie, true);

                    if(isset($_POST["publier"]) && $sortie->getEtat()=='Créée'){
                        $sortie->setEtat($etatRepository->findOneBy(['libelle'=>'Ouverte']));
                        $em->persist($sortie);
                        $em->flush();

                    }
                    return $this->redirectToRoute('app_sortie_index', [], Response::HTTP_SEE_OTHER);
                }

                return $this->renderForm('sortie/edit.html.twig', [
                    'sortie' => $sortie,
                    'form' => $form,
                    'villes' => $villes,
                ]);

            }
            /*elseif($this->getUser()->getId() == $sortie->getOwner()->getId()) {
                $form = $this->createForm(SortieType::class, $sortie);
                $form->handleRequest($request);

                if ($form->isSubmitted() && $form->isValid()) {
                    $sortieRepository->save($sortie, true);

                    return $this->redirectToRoute('app_sortie_index', [], Response::HTTP_SEE_OTHER);
                }

                return $this->renderForm('sortie/edit.html.twig', [
                    'sortie' => $sortie,
                    'form' => $form,
                    'villes' => $villes,
                ]);*/

        }
        return $this->render('sortie/index.html.twig', [
            'sorties' => $sortieRepository->findAll(),
        ]);
    }

    #[Route('/{id}', name: 'app_sortie_delete', methods: ['POST'])]
    public function delete(Request $request, Sortie $sortie, SortieRepository $sortieRepository): Response
    {
        if($this->getUser() != null){
            // Check if the connected user is the owner or is admin
            if ( in_array( 'ROLE_ADMIN' , $this->getUser()->getRoles())) {
                if ($this->isCsrfTokenValid('delete' . $sortie->getId(), $request->request->get('_token'))) {
                    $sortieRepository->remove($sortie, true);
                }
            }
        }

        return $this->redirectToRoute('app_sortie_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id}/subscribe', name: 'app_sortie_subscribe', methods: ['GET','POST'])]
    public function subscribe(Request $request, Sortie $sortie, SortieRepository $sortieRepository, EtatRepository $etatRepository, EntityManagerInterface $em): Response
    {

        if($sortie->getDeadlineSubscription()>new \DateTimeImmutable("now")
            && count($sortie->getParticipants())<$sortie->getMaxSubscription()) {
            if ($this->getUser() != null) {
                // Check if the connected user is connected
                $sortie->addParticipant($this->getUser());
                if ($sortie->getMaxSubscription()==count($sortie->getParticipants())){
                    $sortie->setEtat($etatRepository->findOneBy(['libelle' => 'Clôturée']));
                }
                $em->persist($sortie);
                $em->flush();
            }
        }
        //TODO error or flash message if not possible to subscribe
        return $this->redirectToRoute('app_sortie_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id}/unsubscribe', name: 'app_sortie_unsubscribe', methods: ['GET','POST'])]
    public function unsubscribe(Request $request, Sortie $sortie, SortieRepository $sortieRepository, EtatRepository $etatRepository, EntityManagerInterface $em): Response
    {
        if($sortie->getDeadlineSubscription()>new \DateTimeImmutable("now")) {
            if ($this->getUser() != null) {
                // Check if the connected user is connected
                $sortie->removeParticipant($this->getUser());
                $sortie->setEtat($etatRepository->findOneBy(['libelle'=>'Ouverte']));
                $em->persist($sortie);
                $em->flush();
            }

        }
        return $this->redirectToRoute('app_sortie_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id}/canceling', name: 'app_sortie_canceling', methods: ['GET','POST'])]
    public function canceling(Request $request, Sortie $sortie, SortieRepository $sortieRepository, EntityManagerInterface $em): Response{
        //TODO : vérifier l'utilisateur= organisateur/admin
        $id=$request->get("id");
        return $this->render('sortie/cancel.html.twig', [
            'sortie' => $sortieRepository->find($id),
        ]);
    }
    #[Route('/app/sortie/cancel/{id}', name: 'app_sortie_cancel', methods: ['GET','POST'])]
    public function cancel(Request $request, Sortie $sortie, SortieRepository $sortieRepository, EtatRepository $etatRepository, EntityManagerInterface $em): Response{

        $id=$request->get("id");
        $sortie=$sortieRepository->find($id);
        $etat=$etatRepository->findOneBy(['libelle'=>'Annulée']);

        if ($this->getUser()==$sortie->getOwner() ||
            (in_array( 'ROLE_ADMIN' , $this->getUser()->getRoles()))
            && $sortie->getDeadlineSubscription()>new \DateTimeImmutable("now")
            && $sortie->getEtat()->getLibelle()!=='Annulée'){

            if(isset($_POST['motif'])) {
                $sortie->setMotif($_POST['motif']);
                $sortie->setEtat($etat);
                $em->persist($sortie);
                $em->flush();
            }else{
                return $this->render('sortie/cancel.html.twig', [
                    'sortie' => $sortieRepository->find($id),
                ]);
            }
        }
        return $this->redirectToRoute('app_sortie_index', [], Response::HTTP_SEE_OTHER);

    }
}

