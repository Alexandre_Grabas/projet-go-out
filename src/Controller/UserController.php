<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\SiteRepository;
use App\Repository\UserRepository;
use App\Service\FileUploader;
use Doctrine\ORM\EntityManagerInterface;
use League\Csv\Reader;
use League\Csv\Info;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;


class UserController extends AbstractController
{
    private EntityManagerInterface $em;
    #[Route('/admin/user', name: 'app_user_index', methods: ['GET'])]
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /*
    #[Route('/new', name: 'app_user_new', methods: ['GET', 'POST'])]
    public function new(Request $request, UserRepository $userRepository): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userRepository->save($user, true);

            return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('user/new.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);
    }
    */

    #[Route('/app/user/{id}', name: 'app_user_show', methods: ['GET'])]
    public function show(User $user): Response
    {
        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    #[Route('/app/user/{id}/edit', name: 'app_user_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, User $user, UserRepository $userRepository, EntityManagerInterface $em,
                         UserPasswordHasherInterface $userPasswordHasher,FileUploader $fileUploader): Response
    {
    //TODO modification admin + role user
        $form = $this->createForm(UserType::class, $user);

        $id=$request->get("id");

        $temp= clone $user;

        $roles=$temp->getRoles();
        $active=$temp->isActive();
        $isVerified=$temp->isVerified();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //check mot de passe et confirmation identique
            //si pas admin

            if ($this->getUser()===$user) {

                if ($userPasswordHasher->isPasswordValid($user, $form->get('plainPassword')->getData())) {
                    if ((in_array('ROLE_ADMIN', $temp->getRoles()))) {

                        //on check isValid, role, active modifié et redirection si pb

                        $user = $em->getRepository(User::class)->find($id);
                        $user->setEmail($user->getEmail());
                        $user->setFirstname($user->getFirstname());
                        $user->setLastname($user->getLastname());
                        $user->setPhone($user->getPhone());
                        $user->setSite($user->getSite());
                        $user->setRoles($user->getRoles());
                        $user->setActive($user->isActive());
                        $user->setIsVerified($user->isVerified());
                        if ($form->get('nouveauPassword')->getData()!=null){
                            $user->setPassword($userPasswordHasher->hashPassword($user, $form->get('nouveauPassword')->getData()));
                        }else {
                            $user->setPassword($user->getPassword());
                        }
                        $user->setTheme($user->getTheme());

                        // traitement de l'image
                        /** @var UploadedFile $imageFile */
                        $imageFile = $form->get('image')->getData();
                        if (($form->has('deleteImage') && $form['deleteImage']->getData()) || $imageFile) {
                            //suppression de l'image si option cochée dans le formulaire ou si l'image à changée
                            $fileUploader->delete($user->getFilename(), $this->getParameter('app.images_user_directory'));
                            if ($imageFile) {
                                $user->setFilename($fileUploader->upload($imageFile));
                            } else {
                                $user->setFilename(null);
                            }
                        }
                        $em->persist($user);
                        $em->flush();
                    } else {
                        $user = $em->getRepository(User::class)->find($id);
                        $user->setEmail($user->getEmail());
                        $user->setFirstname($user->getFirstname());
                        $user->setLastname($user->getLastname());
                        $user->setPhone($user->getPhone());
                        $user->setSite($user->getSite());
                        $user->setRoles($temp->getRoles());
                        $user->setActive($temp->isActive());
                        $user->setIsVerified($temp->isVerified());
                        if ($form->get('nouveauPassword')->getData()!=null){
                            $user->setPassword($userPasswordHasher->hashPassword($user, $form->get('nouveauPassword')->getData()));
                        }else {
                            $user->setPassword($user->getPassword());
                        }
                        $user->setTheme($user->getTheme());
                        // traitement de l'image

                        $imageFile = $form->get('image')->getData();
                        if (($form->has('deleteImage') && $form['deleteImage']->getData()) || $imageFile) {
                            //suppression de l'image si option cochée dans le formulaire ou si l'image à changée
                            $fileUploader->delete($user->getFilename(), $this->getParameter('app.images_user_directory'));
                            if ($imageFile) {
                                $user->setFilename($fileUploader->upload($imageFile));
                            } else {
                                $user->setFilename(null);
                            }
                        }
                        $em->persist($user);
                        $em->flush();
                    }
                }
            }
            //si admin
            elseif ((in_array( 'ROLE_ADMIN' , $this->getUser()->getRoles())) && !($this->getUser()===$user)){
                //dd("admin");

                $user=$em->getRepository(User::class)->find($id);
                $user->setEmail($user->getEmail());
                $user->setFirstname($user->getFirstname());
                $user->setLastname($user->getLastname());
                $user->setPhone($user->getPhone());
                $user->setSite($user->getSite());
                $user->setRoles($user->getRoles());
                $user->setActive($user->isActive());
                $user->setIsVerified($user->isVerified());
                $user->setPassword($temp->getPassword());
                $user->setTheme($user->getTheme());
                // traitement de l'image

                $imageFile = $form->get('image')->getData();
                if (($form->has('deleteImage') && $form['deleteImage']->getData()) || $imageFile) {
                    //suppression de l'image si option cochée dans le formulaire ou si l'image à changée
                    $fileUploader->delete($user->getFilename(), $this->getParameter('app.images_user_directory'));
                    if ($imageFile) {
                        $user->setFilename($fileUploader->upload($imageFile));
                    } else {
                        $user->setFilename(null);
                    }
                }
                $em->persist($user);
                $em->flush();
            }

            return $this->redirectToRoute('app_user_show', ['id'=>$user->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('user/edit.html.twig', [
            'user' => $user,
            'form' => $form,
        ]);
    }

    #[Route('/admin/user/{id}/delete', name: 'app_user_delete', methods: ['POST'])]
    public function delete(Request $request, User $user, UserRepository $userRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            // si image associée alors suppression de celle ci
            $fileUploader = new FileUploader($this->getParameter('app.images_user_directory'));
            $imageFile = $user->getFilename();
            if(null != $imageFile){
                $fileUploader->delete( $imageFile, $this->getParameter('app.images_user_directory'));
            }

            $userRepository->remove($user, true);
        }

        return $this->redirectToRoute('app_home', [], Response::HTTP_SEE_OTHER);
    }
    #[Route('/admin/user/insert', name: 'app_users_insert', methods: ['GET','POST'])]
    public function insertForm(): Response
    {
        return $this->render('user/insert.html.twig');
    }
    #[Route('/admin/user/insertRead', name: 'app_users_insert_read' , methods: ['POST'])]
    public function insertRead(Request $request, UserRepository $userRepository,SiteRepository $siteRepository,UserPasswordHasherInterface $userPasswordHasher,EntityManagerInterface $em): Response
    {
        $file = $request->files->get('csv_users');

        $content = file_get_contents($file);
        //récupération de l'entête de fichier caché (BOM)
        $bom =Info::fetchBOMSequence($content);
        //Suppression du BOM de la chaine de caractères avant le READER
        $content=str_replace($bom,"",$content);
        $csv = Reader::createFromString($content);
        //caractère de séparation de champ du .csv
        //TODO caractère de séparation de champ du .csv à récupérer de manière indépendante
        $csv->setDelimiter(',');
        //la 1ère ligne du .csv détermine l'ordre des infos.
        $csv->setHeaderOffset(0);

        $userCreated = 0;

        foreach ($csv as  $arrayUser){

            $user = $userRepository->findOneBy(['email'=>$arrayUser['email']]);
            if(!$user){
                //s'il n'existe pas on le créé
                $user=new User();
            }

            $hashedPassword = $userPasswordHasher->hashPassword($user,$arrayUser['password']);
            //email,roles,password,firstname,lastname,phone,active,site,isVerified,theme,filename=non modifié
            $user->setEmail($arrayUser['email'])
                ->setRoles([$arrayUser['roles']])
                ->setPassword($hashedPassword)
                ->setFirstname($arrayUser['firstname'])
                ->setLastname($arrayUser['lastname'])
                ->setPhone($arrayUser['phone'])
                ->setActive($arrayUser['active'])
                ->setSite($siteRepository->findOneBy(['name'=>$arrayUser['site']]))
                ->setIsVerified($arrayUser['isVerified'])
                ->setTheme($arrayUser['theme']);


            $em->persist($user);
            $em->flush();

            ++$userCreated;
        }

        return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);

    }


}


