<?php

namespace App\Command;

use App\Service\ImportCsvUsersService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(name: 'app:import-users')]
class ImportUsersCommand extends Command {

    public function __construct(
        private ImportCsvUsersService $importCsvUsersService,
    )
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input,$output);
        $io->title('Importation des Users :');
        $this->importCsvUsersService->importUsers($io);

        return Command::SUCCESS;
    }


}