<?php

namespace App\DataFixtures;

use App\Entity\Etat;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class EtatFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        $etat= new Etat();
        $etat->setLibelle("Créée");
        $manager->persist($etat);
        $etat2= new Etat();
        $etat2->setLibelle("Ouverte");
        $manager->persist($etat2);
        $etat3= new Etat();
        $etat3->setLibelle("Clôturée");
        $manager->persist($etat3);
        $etat4= new Etat();
        $etat4->setLibelle("En cours");
        $manager->persist($etat4);
        $etat5= new Etat();
        $etat5->setLibelle("Passée");
        $manager->persist($etat5);
        $etat6= new Etat();
        $etat6->setLibelle("Annulée");
        $manager->persist($etat6);

        $this->addReference("ouverte",$etat2);


        $manager->flush();
    }
}
