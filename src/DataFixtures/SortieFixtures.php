<?php

namespace App\DataFixtures;

use App\Entity\Sortie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class SortieFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);

        $s1 = new Sortie();
        $s1->setName('Nouvel an');
        $s1->setDescription('Soirée du nouvel an');
        $s1->setDuration('240');
        $s1->setSite($this->getReference("enirennes"));
        $s1->setEtat($this->getReference("ouverte"));
        $s1->setDateHourStart(new \DateTimeImmutable('2023-12-31'));
        $s1->setDeadlineSubscription(new \DateTimeImmutable('2023-11-31'));
        $s1->setLieu($this->getReference('pub'));
        $s1->setMaxSubscription(3);
        $s1->setOwner($this->getReference('user'));

        $manager->persist($s1);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [SiteFixtures::class,
                EtatFixtures::class,
                LieuFixtures::class,
                UserFixtures::class,
            ];
    }
}
