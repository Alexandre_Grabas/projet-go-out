<?php

namespace App\DataFixtures;

use App\Entity\Lieu;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class LieuFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $lieu1 = new Lieu();
        $lieu1->setName("ENI Rennes");
        $lieu1->setVille($this->getReference("rennes"));
        $lieu1->setAdress("8 Rue Léo Lagrange");
        $lieu1->setLatitude(48.0389009);
        $lieu1->setLongitude(-1.69238);
        $manager->persist($lieu1);

        $lieu2 = new Lieu();
        $lieu2->setName("O'Connell's Irish Pub");
        $lieu2->setVille($this->getReference("rennes"));
        $lieu2->setAdress("6 place du parlement");
        $lieu2->setLatitude(48.1120701);
        $lieu2->setLongitude(-1.6772984);
        $manager->persist($lieu2);

        $this->addReference("pub", $lieu2);

        $manager->flush();
    }


    public function getDependencies()
    {
        return [VilleFixtures::class];
    }
}
