<?php

namespace App\DataFixtures;

use App\Entity\Ville;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class VilleFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $ville1 = new Ville();
        $ville1->setName("RENNES");
        $ville1->setPostcode("35000");
        $this->addReference("rennes",$ville1);
        $manager->persist($ville1);

        $ville2 = new Ville();
        $ville2->setName("NIORT");
        $ville2->setPostcode("79000");
        $this->addReference("niort",$ville2);
        $manager->persist($ville2);

        $ville3 = new Ville();
        $ville3->setName("NANTES");
        $ville3->setPostcode("44000");
        $this->addReference("nantes",$ville3);
        $manager->persist($ville3);

        $manager->flush();
    }
}
