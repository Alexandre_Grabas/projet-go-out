<?php

namespace App\DataFixtures;

use App\Entity\Site;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        /* ADMIN */
        $admin= new User();
        $admin->setRoles(['ROLE_USER','ROLE_ADMIN']);
        $admin->setFirstname('admin');
        $admin->setLastname('admin');
        $admin->setEmail('admin@admin.fr');
        $admin->setPhone('001122334455');
        //password is 'admin'
        $admin->setPassword('$2y$13$qfhnWVAVBksthrJ2gg6gw.AUt04GJ20HLJ/vgTMAOc796D6Vd.9Ly');
        $admin->setSite($this->getReference("enirennes"));
        $admin->setActive(true);
        $admin->setIsVerified(true);
        $admin->setTheme('lightblue');
        $manager->persist($admin);

        /* USER */
        $user= new User();
        $user->setRoles(['ROLE_USER']);
        $user->setFirstname('user');
        $user->setLastname('user');
        $user->setEmail('user@user.fr');
        $user->setPhone('001122334455');
        //password is 'user'
        $user->setPassword('$2y$13$7BAfbetLFC25oWRSe6pfgupHZIzhZyxcv03n.gwpEVzCHVE2SQghm');
        $user->setSite($this->getReference("enirennes"));
        $user->setActive(true);
        $user->setIsVerified(true);
        $user->setTheme('flower');
        $manager->persist($user);

        $this->addReference('user', $user);

        $manager->flush();
    }


    public function getDependencies()
    {
        return [SiteFixtures::class];
    }
}
