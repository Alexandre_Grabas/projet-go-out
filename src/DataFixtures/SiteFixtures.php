<?php

namespace App\DataFixtures;

use App\Entity\Site;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class SiteFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);
        $site=new Site();
        $site->setName('ENI RENNES');
        $this->addReference("enirennes",$site);

        $site2=new Site();
        $site2->setName('ENI NANTES');
        $this->addReference("eninantes",$site2);

        $manager->persist($site);
        $manager->persist($site2);


        $manager->flush();
    }

    public function getDependencies()
    {
        return [EtatFixtures::class];
    }
}
