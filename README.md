# GO OUT
_A project about making a website to schedule outings._

## Context
This is a group project given by french school ENI to the students to practice website creation with Symfony.

## Goals
* Create a website ;
* Follow client documentation ;
* Teamwork ;

## Features
* User
  * SignUp, SignIn, SignOut ;
  * Some private data (e.g. name, phone) ;
  * Profile management ;
  * Avatar image.
* Outing
  * Creation, management, deletion ;
  * Filters on the display list ;
  * Geographical data (City, Location) ;
  * User (un)subscription.
* Admin
  * Upload users through CSV ;
  * Panels to change virtually everything from the website.

## Additionnal Features
* User
  * Colour themes ;

## Environment

### TOOLS
* WAMP Server - v. 3.3 
  * PHP - v. 8.1.
  * MySQL - v. 8.0.
  * Apache Server - v. 2.4.54
* Symfony - v. 6.3
* Composer - v. 2.5
* PowerShell - v. 5.1
* Scoop
* Papercut
* Git Lab

### SETUP
1. Install WAMP, PowerShell, Git, Papercut, Composer
2. Install your favorite IDE and setup it for Symfony
3. Install Scoop through PowerShell (admin)
   * >Set-ExecutionPolicy RemoteSigned -Scope CurrentUser
   * >irm get.scoop.sh | iex
4. Install Symfony CLI through PowerShell  
   * >scoop install symfony-cli
5. Clone Project
   * >https://gitlab.com/Alexandre_Grabas/goout.git
6. Add dependencies through Composer
   * >composer install
7. Create void database with PhpMyAdmin
   * User : 'root' / [no password]
   * Name : 'goout'
   * Encoding : utf8mb4_general_ci
8. Start WAMP Server
9. Run Doctrine setup
   * >symfony console doctrine:migrations:migrate
   * >symfony console doctrine:fixtures:load
10. Start Symfony local development server
    * >symfony server:start -d
11. Go on your freshly hosted website
    * >http://127.0.0.1:8000
12. Enjoy !
