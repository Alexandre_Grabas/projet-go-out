function displayMenu(){
    let menu = document.getElementById('main-menu-display');
    let btn = document.getElementById('main-menu-burger-icon');

    menu.classList.toggle('main-menu-display-opened');
    btn.classList.toggle('main-menu-burger-icon-opened');
}